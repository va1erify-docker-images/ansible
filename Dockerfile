FROM python:3-alpine

ENV ANSIBLE_FORCE_COLOR="true"
ENV ANSIBLE_HOST_KEY_CHECKING="false"

RUN apk add --update --no-cache \
    git \
    openssh-client \
    rsync \
    ansible-lint \
    unzip \
    tar \
    zip

RUN mkdir ~/.ssh && \
    ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts

RUN apk add --update --no-cache \
    --virtual .build-deps \
    make \
    gcc \
    libffi-dev \
    musl-dev \
    && python3 -m pip install --no-cache-dir ansible \
    && python3 -m pip install --no-cache-dir jmespath \
    && python3 -m pip install --no-cache-dir git+https://github.com/ansible-community/molecule \
    && python3 -m pip install --no-cache-dir pytest-testinfra \
    && apk del .build-deps

RUN apk add sshpass --repository https://dl-cdn.alpinelinux.org/alpine/edge/community


#ENTRYPOINT ["/bin/ash"]